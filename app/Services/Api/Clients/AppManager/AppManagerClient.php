<?php
namespace App\Services\Api\Clients\AppManager;

use App\Services\Api\Clients\AppManager;
use Illuminate\Support\Facades\Config;

class AppManagerClient extends Abstracts\AppManagerApi implements Contracts\Content
{

	protected $url;

	public function __construct()
	{
		$this->url = Config::get('api.api_url');
	}

	public function getAds()
	{
		return $this->get('ads');
	}
}
