<?php
namespace App\Services\Api\Clients\AppManager\Abstracts;

abstract class AppManagerApi
{

	protected $routes;

	protected $url;

	public function get(string $route = null)
	{
		return $this->request($this->url.$route);
	}

	private function request(string $endpoint = null)
	{
      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $endpoint);
      curl_setopt($ch, CURLOPT_POST, 0);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
      curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
      curl_setopt($ch, CURLOPT_HTTPAUTH, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

      $response = curl_exec($ch);

      $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      if (isset($this->http_errors[$http_code]))
      {
          throw new \Exception('Response Http Error - ' . $this->http_errors[$http_code]);
      }

      if (0 < curl_errno($ch))
      {
          throw new \Exception('Unable to connect to ' .$endpoint . ' Error: ' . curl_error($ch));
      }

      curl_close($ch);

      return $response;
	}
}
