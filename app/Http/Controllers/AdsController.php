<?php

namespace App\Http\Controllers;

use App\Services\Api\Clients\AppManager\AppManagerClient;
use App\Http\Resources\Ads as AdsResources;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class AdsController extends Controller
{

    private $apiClient;

    public function __construct(AppManagerClient $apiClient, AdsResources $adsResource)
    {
        $this->apiClient = $apiClient;
        $this->adsResource = $adsResource;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $json = $this->apiClient->getAds();
      $array = $this->adsResource->toArray($json);

      $collection = collect($array['data']);
      $ids = $collection->pluck('id');
      $cities = $collection->pluck('cities', 'id');
      $title = $collection->pluck('title', 'id');

      foreach ($ids as $idsItem) {
        $output[$idsItem] = [
          'id' => $idsItem,
          'cities' => $cities[$idsItem],
          'title' => $title[$idsItem]
        ];
      }

      return $output;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
