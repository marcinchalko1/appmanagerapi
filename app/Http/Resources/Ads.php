<?php

namespace App\Http\Resources;

class Ads
{

    public function toArray(string $request = null)
    {
        return json_decode($request, true);
    }
}
