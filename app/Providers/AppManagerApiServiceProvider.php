<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Api\Clients\AppManager\AppManagerClient;

class AppManagerApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind(
        'App\Services\Api\Clients\AppManager\AppManagerClient',
        function ($app) {
          return new AppManagerClient();
        }
      );
    }
}
